function onDeviceReady(){
    var fileName = 'logo.png';
    var sourceFilePath = cordova.file.applicationDirectory+'www/img/'+fileName;
    var targetDirPath = cordova.file.documentsDirectory;
    var targetFilePath = targetDirPath + fileName;

    loadImage(sourceFilePath);

    window.resolveLocalFileSystemURL(targetFilePath, function (targetFileEntry) {
        loadImage(targetFilePath);
    }, function(){
        window.resolveLocalFileSystemURL(sourceFilePath, function (sourceFileEntry) {
            window.resolveLocalFileSystemURL(targetDirPath, function (targetDirEntry) {
                sourceFileEntry.copyTo(targetDirEntry, 'logo.png', function(){
                    loadImage(targetFilePath);
                }, onError);
            }, onError);
        }, onError);
    });

}

function onError(error){
    console.error("File Error: "+JSON.stringify(error));
}

function loadImage(path){
    var image = new Image();
    image.onerror = function(){
        log("Error - Unable to load image: '"+path+"'");
    };
    image.onload = function(){
        log("Loaded image: '"+path+"'");
    };
    image.src = path;
}

function log(msg){
    console.log(msg);
    document.body.innerHTML += '<p>'+msg+'</p>';
}

document.addEventListener('deviceready', onDeviceReady, false);